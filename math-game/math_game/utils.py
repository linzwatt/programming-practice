import time
import string


ALPHABET_LOWER = list(string.ascii_lowercase)
QUIT_SENTINELS = ["quit", "exit", "bye"]


class UserQuitException(Exception):
    pass


def get_current_time():
    return time.perf_counter()
