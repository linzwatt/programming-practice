import random
import typing as tp
from termcolor import cprint, colored

from .question import MathQuestionFactory, MathQuestion
from .score import ScoreKeeper, SimpleScoreKeeper, TimedScoreKeeper
from .utils import QUIT_SENTINELS, UserQuitException


class MathGame:
    def __init__(self, questions: tp.List[MathQuestion], score_keeper: ScoreKeeper = SimpleScoreKeeper()):
        self.questions = questions
        self.score_keeper = score_keeper

    def run(self):
        try:
            cprint("Welcome to the Math Game!", "green")
            cprint("Answer `quit` to exit the game early.", "grey")
            self.score_keeper.start()

            # iterate through the questions
            for qi, question in enumerate(self.questions):
                # print header and question
                cprint("\n" + ("=" * 40), "grey")
                cprint(f"\nQuestion {qi + 1}/{len(self.questions)}:", "green")
                print(question.question)

                # while input is invalid, keep asking for input
                while True:
                    user_input = input(colored("Your Answer: ", "blue")).strip()

                    # check if user wants to quit
                    if user_input in QUIT_SENTINELS:
                        raise UserQuitException("User quit the game.")

                    # check the answer
                    result = question.check_answer(user_input)

                    # provide feedback
                    cprint(result.message, "green" if result.correct else "yellow")

                    # allow user to try again
                    if result.try_again:
                        continue

                    break

                # update score
                self.score_keeper.update_single_question(question, result)

        except KeyboardInterrupt:
            cprint("\nGame quit by Ctrl+C", "red")

        except UserQuitException:
            cprint("\nGame quit by user", "red")

        finally:
            cprint(f"\n{self.score_keeper.report}", "green")


if __name__ == "__main__":
    num_questions = 20
    max_value = 2

    score_keeper = TimedScoreKeeper()

    questions = []
    for _ in range(num_questions):
        multi_choice = random.choice([True, False])
        question_type = random.choice(["addition", "multiplication"])
        operands = [random.randint(0, max_value) for _ in range(2)]
        questions.append(MathQuestionFactory.create_question(question_type, *operands, multi_choice=multi_choice))

    game = MathGame(questions, score_keeper)
    game.run()
