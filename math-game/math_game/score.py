import time
import typing as tp

from abc import ABC, abstractmethod

from .types import Numerical
from .utils import get_current_time
from .question import MathQuestion, MathQuestionResult


class ScoreKeeper(ABC):
    def start(self):
        self.start_time = get_current_time()

    @property
    @abstractmethod
    def fractional_score(self) -> tp.Tuple[int, int]:
        raise NotImplementedError

    @property
    @abstractmethod
    def percentage_score(self) -> Numerical:
        raise NotImplementedError

    @property
    def summary_string(self) -> str:
        fractional = self.fractional_score
        if len(fractional) != 2:
            raise ValueError(f"Fractional score should have 2 elements - got: {fractional}")
        if fractional[1] == 0:
            return "Unable to compute score - no questions asked"
        return f"{fractional[0]}/{fractional[1]} ({self.percentage_score:.3g}%)"

    @property
    @abstractmethod
    def report(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def update_single_question(self, question: MathQuestion, result: MathQuestionResult):
        raise NotImplementedError


class SimpleScoreKeeper(ScoreKeeper):
    def __init__(self):
        self.num_correct = 0
        self.num_asked = 0

    @property
    def fractional_score(self) -> tp.Tuple[Numerical, Numerical]:
        return self.num_correct, self.num_asked

    @property
    def percentage_score(self) -> Numerical:
        return self.num_correct / self.num_asked * 100

    def update_single_question(self, question: MathQuestion, result: MathQuestionResult):
        if result.correct:
            self.num_correct += 1
        self.num_asked += 1

    @property
    def report(self):
        return f"Your Score: {self.summary_string}"


class TimedScoreKeeper(ScoreKeeper):
    def __init__(self):
        self.num_correct = 0
        self.num_asked = 0
        self.history = []

    @property
    def fractional_score(self) -> tp.Tuple[Numerical, Numerical]:
        return self.num_correct, self.num_asked

    @property
    def percentage_score(self) -> Numerical:
        return self.num_correct / self.num_asked * 100

    def update_single_question(self, question: MathQuestion, result: MathQuestionResult):
        if result.correct:
            self.num_correct += 1
        self.num_asked += 1

        if not self.start_time:
            raise RuntimeError("ScoreKeeper not started")

        question_start_time = self.history[-1]["end_time"] if self.history else self.start_time
        time_now = get_current_time()
        self.history.append({
            "question": question,
            "result": result,
            "time_taken": time_now - question_start_time,
            "end_time": time_now
        })

    @property
    def report(self):
        report = f"Score: {self.summary_string}"

        if self.history:
            report += f"\nTotal time taken: {self.history[-1]['end_time'] - self.start_time:.3g}s"

            # print the average time per question
            report += (
                f"\nAverage time per question: "
                f"{(self.history[-1]['end_time'] - self.start_time) / self.num_asked:.3g}s"
            )

            # print a breakdown of average time taken for each question type
            for question_type in list(set(e["question"].question_type for e in self.history)):
                times = [
                    e["time_taken"] for e in self.history if e["question"].question_type == question_type
                ]
                report += f"\nAverage time for {question_type.value}: {sum(times) / len(times):.3g}s"

        return report
