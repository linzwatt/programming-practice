from enum import Enum
import random
import typing as tp

from abc import ABC, abstractmethod
from dataclasses import dataclass

from .utils import ALPHABET_LOWER
from .types import Numerical


class QuestionType(str, Enum):
    ADDITION = "Addition"
    MULTIPLICATION = "Multiplication"


@dataclass
class MathQuestionResult:
    message: str
    correct: bool
    try_again: bool = False


class MathQuestion(ABC):
    @property
    @abstractmethod
    def question_type(self) -> QuestionType:
        raise NotImplementedError

    @property
    @abstractmethod
    def question(self) -> str:
        raise NotImplementedError

    @property
    @abstractmethod
    def answer(self):
        raise NotImplementedError

    @property
    def answer_display(self):
        return self.answer

    @staticmethod
    def clean_input(user_input: str) -> str:
        return user_input.strip()

    @staticmethod
    def is_numerical(user_input: str) -> bool:
        try:
            _ = float(user_input)
            return True
        except ValueError:
            return False

    def check_answer(self, user_input: str, cast_fn: tp.Callable = float) -> MathQuestionResult:
        user_input = self.clean_input(user_input)

        if not self.is_numerical(user_input):
            return MathQuestionResult("Invalid input - please provide a numerical answer", False, True)

        try:
            user_answer = cast_fn(user_input)
        except Exception as e:
            return MathQuestionResult("Error evaluating answer - please provide a numerical answer", False, True)

        if user_answer == self.answer:
            return MathQuestionResult("Correct! :)", True)

        return MathQuestionResult(f"Incorrect! :( The correct answer was: {self.answer_display}.", False)


class MathQuestionFactory:
    @staticmethod
    def create_question(question_type: str, *args, multi_choice: bool = False) -> MathQuestion:
        question = None

        match question_type:
            case "addition":
                question = AdditionQuestion(*args)
            case "multiplication":
                question = MultiplicationQuestion(*args)

        if not question:
            raise ValueError(f"Invalid question type: {question_type}")

        if multi_choice:
            question = MultiChoiceQuestionRandomised(question)

        return question


class AdditionQuestion(MathQuestion):
    def __init__(self, *args):
        self.operands = list(args)

    @property
    def question_type(self) -> QuestionType:
        return QuestionType.ADDITION

    @property
    def question(self) -> str:
        return f"What's {' + '.join(str(x) for x in self.operands)}?"

    @property
    def answer(self) -> Numerical:
        return sum(self.operands)


class MultiplicationQuestion(MathQuestion):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    @property
    def question_type(self) -> QuestionType:
        return QuestionType.MULTIPLICATION

    @property
    def question(self) -> str:
        return f"What's {self.a} * {self.b}?"

    @property
    def answer(self) -> Numerical:
        return self.a * self.b


class MultiChoiceQuestionRandomised(MathQuestion):
    def __init__(self, question: MathQuestion, n_choices: int = 4):
        self.math_question = question
        self.n_choices = n_choices
        if self.n_choices > len(ALPHABET_LOWER):
            raise ValueError(f"Too many choices - got: {self.n_choices}, max: {len(ALPHABET_LOWER)}")
        self._prepare_answers()

    def _prepare_answers(self):
        self.answers = [self.math_question.answer]
        for _ in range(self.n_choices - 1):
            while True:
                random_answer = self.math_question.answer + random.randint(-5, 5)
                if random_answer not in self.answers:
                    break
            self.answers.append(random_answer)
        random.shuffle(self.answers)

    @property
    def question_type(self) -> QuestionType:
        return self.math_question.question_type

    @property
    def question(self) -> str:
        question = f"{self.math_question.question}\n"

        for i, answer in enumerate(self.answers):
            question += f"[{ALPHABET_LOWER[i]}] {answer}\n"

        return question

    @property
    def answer(self) -> str:
        return ALPHABET_LOWER[self.answers.index(self.math_question.answer)]

    @property
    def answer_display(self) -> str:
        return f"[{self.answer}] {self.math_question.answer}"

    def check_answer(self, user_input: str) -> MathQuestionResult:
        user_input = self.clean_input(user_input).lower()

        if len(user_input) != 1 or (user_input not in ALPHABET_LOWER):
            return MathQuestionResult("Invalid input - please provide a single letter answer", False, True)

        if user_input == self.answer:
            return MathQuestionResult("Correct! :)", True)

        return MathQuestionResult(f"Incorrect! :( The correct answer was {self.answer_display}.", False)
